﻿using HueCIT.Modules.ChuyenDoiSoTrinhDien.Interface.DangKyGiaiPhap;
using HueCIT.Modules.ChuyenDoiSoTrinhDien.Models;
using HueCIT.Modules.ChuyenDoiSoTrinhDien.Repository.DangKyGiaiPhap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace HueCIT.Modules.ChuyenDoiSoTrinhDien.Services
{
    [AllowAnonymous]
    public class DangKyGiaiPhapApiController :ApiBaseController
    {
        private readonly IDangKyGiaiPhapRepository _repository;
        public DangKyGiaiPhapApiController()
        {
            _repository = new DangKyGiaiPhapRepository();
        }

        [HttpPost]
        public async Task<HttpResponseMessage> GetsLinhVuc(SearchLinhVuc data)
        {
            try
            {
                ApiResult<List<LinhVuc>> lst = new ApiResult<List<LinhVuc>>();
                lst = await _repository.GetsLinhVuc(data);
                return Request.CreateResponse(HttpStatusCode.OK, lst, "application/json");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Lỗi :" + ex.Message, "application/json");
            }
        }

        [HttpPost]
        public async Task<HttpResponseMessage> AddDangKyGiaiPhap(DangKyGiaiPhapProperties data)
        {
            try
            {
                ApiResult<DangKyGiaiPhapProperties> lst = new ApiResult<DangKyGiaiPhapProperties>();
                lst = await _repository.AddDangKyGiaiPhap(data);
                return Request.CreateResponse(HttpStatusCode.OK, lst, "application/json");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Lỗi :" + ex.Message, "application/json");
            }
        }

        [HttpPost]
        public async Task<HttpResponseMessage> GetsDangKyGiaiPhap(DangKyFilterLinhVuc data)
        {
            try
            {
                ApiResult<List<DangKyGiaiPhapGet>> lst = new ApiResult<List<DangKyGiaiPhapGet>>();
                lst = await _repository.GetsDangKyGiaiPhap(data);
                return Request.CreateResponse(HttpStatusCode.OK, lst, "application/json");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Lỗi :" + ex.Message, "application/json");
            }
        }

        [HttpPost]
        public async Task<HttpResponseMessage> GetsLinhVucGiaiPhap()
        {
            try
            {
                ApiResult<List<LinhVuc>> lst = new ApiResult<List<LinhVuc>>();
                lst = await _repository.GetsLinhVuc_GiaiPhap();
                return Request.CreateResponse(HttpStatusCode.OK, lst, "application/json");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Lỗi :" + ex.Message, "application/json");
            }
        }

        [HttpPost]
        public async Task<HttpResponseMessage> GetGiaiPhapNew()
        {
            try
            {
                ApiResult<DangKyGiaiPhapGet> lst = new ApiResult<DangKyGiaiPhapGet>();
                lst = await _repository.GetGiaiPhapNew();
                return Request.CreateResponse(HttpStatusCode.OK, lst, "application/json");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Lỗi :" + ex.Message, "application/json");
            }
        }

        [HttpPost]
        public async Task<HttpResponseMessage> GetsGiaiPhapTieuDiem()
        {
            try
            {
                ApiResult<List<DangKyGiaiPhapGet>> lst = new ApiResult<List<DangKyGiaiPhapGet>>();
                lst = await _repository.GetsGiaiPhapTieuDiem();
                return Request.CreateResponse(HttpStatusCode.OK, lst, "application/json");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Lỗi :" + ex.Message, "application/json");
            }
        }

        [HttpPost]
        public async Task<HttpResponseMessage> GetSanPhamGiaiPhap(Guid id)
        {
            try
            {
                ApiResult<DangKyGiaiPhapGet> lst = new ApiResult<DangKyGiaiPhapGet>();
                lst = await _repository.GetSanPhamGiaiPhap(id);
                return Request.CreateResponse(HttpStatusCode.OK, lst, "application/json");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Lỗi :" + ex.Message, "application/json");
            }
        }
    }   
}