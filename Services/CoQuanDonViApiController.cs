﻿using HueCIT.Modules.ChuyenDoiSoTrinhDien.Models;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using EsbUsers.Sso;
using DotNetNuke.Security.Roles;
using System.Collections;
using DotNetNuke.Entities.Portals;
using System.Web.Security;
using DotNetNuke.Entities.Users;
using DotNetNuke.Services.Exceptions;

namespace HueCIT.Modules.ChuyenDoiSoTrinhDien.Services
{
    [AllowAnonymous]
    public class CoQuanDonViApiController : ApiBaseController
    {
        [HttpGet]
        public async Task<HttpResponseMessage> DanhSachCoQuan()
        {
            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                       | SecurityProtocolType.Tls11
                       | SecurityProtocolType.Tls12
                       | SecurityProtocolType.Ssl3;
                var client = new RestClient("https://sohoa.thuathienhue.gov.vn/data/danhsach");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("token", "5d26ef51-a0ac-4b47-8021-c88a621e5df6");
                request.AddHeader("Content-Type", "application/json");
                var body = @"{
                    " + "\n" +
                                    @"""serviceid"": ""ChDSB4MYYasSatFNFD4mCw=="",
                    " + "\n" +
                                    @"""thamso"": {
                    " + "\n" +
                                    @"	""tukhoa"": """",
                    " + "\n" +
                                    @"	""nambaocao"": ""0"",
                    " + "\n" +
                                    @"	""bochiso"": ""0"",
                    " + "\n" +
                                    @"	""nhomtieuchiid"": ""0"",
                    " + "\n" +
                                    @"	""tieuchiid"": ""0"",
                    " + "\n" +
                                    @"	""tieuchithanhphan"": ""0"",
                    " + "\n" +
                                    @"	""donvithuchien"": ""0"",
                    " + "\n" +
                                    @"	""donviduyet"": ""0"",
                    " + "\n" +
                                    @"	""hinhthuc"": ""0"",
                    " + "\n" +
                                    @"	""doituong"": ""0"",
                    " + "\n" +
                                    @"	""capapdung"": ""0"",
                    " + "\n" +
                                    @"	""trangthaibaocao"": ""0"",
                    " + "\n" +
                                    @"	""trangthaicongviec"": ""0"",
                    " + "\n" +
                                    @"	""pheduyet"": ""0""
                    " + "\n" +
                                    @"	},
                    " + "\n" +
                                    @"""page"": ""1"",
                    " + "\n" +
                                    @"""perpage"": ""99999""
                    " + "\n" +
                                    @"}";
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                JObject jResponse = JObject.Parse(response.Content.ToString());
                return Request.CreateResponse(HttpStatusCode.OK, jResponse, "application/json");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Lỗi :" + ex.Message, "application/json");
            }
        }

        [HttpGet]
        public async Task<HttpResponseMessage> DanhSachCoQuanGet(string donvithuchien, int capapdung, int tieuchi, int nam)
        {
            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                       | SecurityProtocolType.Tls11
                       | SecurityProtocolType.Tls12
                       | SecurityProtocolType.Ssl3;
                var client = new RestClient("https://sohoa.thuathienhue.gov.vn/data/danhsach");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("token", "5d26ef51-a0ac-4b47-8021-c88a621e5df6");
                request.AddHeader("Content-Type", "application/json");
                var body = @"{
                    " + "\n" +
                                    @"""serviceid"": ""ChDSB4MYYasSatFNFD4mCw=="",
                    " + "\n" +
                                    @"""thamso"": {
                    " + "\n" +
                                    @"	""tukhoa"": """",
                    " + "\n" +
                                    @"	""nambaocao"": """ + nam + @""",
                    " + "\n" +
                                    @"	""bochiso"": ""0"",
                    " + "\n" +
                                    @"	""nhomtieuchiid"":  """ + tieuchi + @""",
                    " + "\n" +
                                    @"	""tieuchiid"": ""0"",
                    " + "\n" +
                                    @"	""tieuchithanhphan"": ""0"",
                    " + "\n" +
                                    @"	""donvithuchien"": """ + donvithuchien + @""",
                    " + "\n" +
                                    @"	""donviduyet"": ""0"",
                    " + "\n" +
                                    @"	""hinhthuc"": ""0"",
                    " + "\n" +
                                    @"	""doituong"": ""0"",
                    " + "\n" +
                                    @"	""capapdung"": """ + capapdung+ @""",
                    " + "\n" +
                                    @"	""trangthaibaocao"": ""0"",
                    " + "\n" +
                                    @"	""trangthaicongviec"": ""0"",
                    " + "\n" +
                                    @"	""pheduyet"": ""0""
                    " + "\n" +
                                    @"	},
                    " + "\n" +
                                    @"""page"": ""1"",
                    " + "\n" +
                                    @"""perpage"": ""99999""
                    " + "\n" +
                                    @"}";
                request.AddParameter("application/json", body, ParameterType.RequestBody);

                IRestResponse response = client.Execute(request);
                JObject jResponse = JObject.Parse(response.Content.ToString());

                //jResponse["data"] = new JArray(jResponse["data"].OrderByDescending(item => (string)item["tendonvithuchien"]));

                return Request.CreateResponse(HttpStatusCode.OK, jResponse, "application/json");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Lỗi :" + ex.Message, "application/json");
            }
        }

        [HttpGet]
        public async Task<HttpResponseMessage> DanhSachCoQuanGetId(int id)
        {
            try
            {
                var clientSSo = ClientSso.Ins.CurrentSessionLoginInfo;
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                       | SecurityProtocolType.Tls11
                       | SecurityProtocolType.Tls12
                       | SecurityProtocolType.Ssl3;
                var client = new RestClient("https://sohoa.thuathienhue.gov.vn/data/chitiet");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("token", "5d26ef51-a0ac-4b47-8021-c88a621e5df6");
                request.AddHeader("Content-Type", "application/json");
                var body = @"{
                    " + "\n" +
                                    @"""serviceid"": ""ChDSB4MYYasSatFNFD4mCw=="",
                    " + "\n" +
                                    @"""id"": """ + id + @""",
                    " + "\n" +
                                    @"}";
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                JObject jResponse = JObject.Parse(response.Content.ToString());
                return Request.CreateResponse(HttpStatusCode.OK, jResponse, "application/json");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Lỗi :" + ex.Message, "application/json");
            }
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetInfoAccount()
        {
            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                       | SecurityProtocolType.Tls11
                       | SecurityProtocolType.Tls12
                       | SecurityProtocolType.Ssl3;
                var client = new RestClient("https://cqs.thuathienhue.gov.vn/taikhoan/list");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("token", "b9aec902-92f9-3d3f-a349-gfce5f10201f");
                request.AddHeader("Content-Type", "application/json");
                var body = @"{
                    " + "\n" +
                             @"""doituongid"": ""1"",
                    " + "\n" +
                             @"""trangthai"": ""-1"",
                    " + "\n" +
                             @"""page"": ""1"",
                    " + "\n" +
                            @"""perpage"": ""999999"",
                    " + "\n" +
                    @"}";
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                JObject jResponse = JObject.Parse(response.Content.ToString());
                return Request.CreateResponse(HttpStatusCode.OK, jResponse, "application/json");
            } catch (Exception ex)
            {
                Exceptions.LogException(ex);
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Lỗi :" + ex.Message, "application/json");
            }
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetCodeSSO()
        {
            try
            {
                // Lấy danh sách các vai trò trong cổng hiện tại
                ModelSSO MaDonViSSO = new ModelSSO();
               
                // Lấy thông tin người dùng hiện tại
                UserInfo currentUser = UserController.Instance.GetCurrentUserInfo();
                if (currentUser != null)
                {
                    //MaDonViSSO.Username = currentUser.Username;
                    // Lấy danh sách các vai trò của người dùng hiện tại
                    IList <UserRoleInfo> userRoles = RoleController.Instance.GetUserRoles(currentUser, true);

                    // Duyệt qua danh sách các vai trò
                    foreach (RoleInfo role in userRoles)
                    {
                            // Truy cập thông tin của vai trò
                            MaDonViSSO.RoleName.Add(role.RoleName);
                            MaDonViSSO.RoleId.Add(role.RoleID);
                        
                    }
                }
                var clientSSo = ClientSso.Ins.CurrentSessionLoginInfo;
                if (clientSSo != null)
                {
                    MaDonViSSO.MaDonViSSO = clientSSo.MaDonVi;
                    MaDonViSSO.Username = clientSSo.TenDangNhap;
                } else
                {
                    MaDonViSSO.Username = currentUser.Username;
                }

                return Request.CreateResponse(HttpStatusCode.OK, MaDonViSSO, "application/json");
            } catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Lỗi :" + ex.Message, "application/json");
            }
        }

        [HttpPost]
        public async Task<HttpResponseMessage> UpdateBaoCao(BaoCaoChiSoThuocTinhParam data)
        {
            try
            {
                var url = "https://sohoa.thuathienhue.gov.vn/data/capnhattruong";
                var token = "5d26ef51-a0ac-4b47-8021-c88a621e5df6";

                var client = new RestClient(url);
                client.Timeout = -1;

                var request = new RestRequest(Method.POST);
                if (!string.IsNullOrEmpty(data.filekiemchung))
                {
                    request.AddHeader("Token", token);
                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("serviceid", "ChDSB4MYYasSatFNFD4mCw==");
                    request.AddParameter("key", "[\"ghichu\", \"noidungbaocao\",\"ngaybaocao\",\"trangthaibaocao\",\"filekiemchung\"]");

                    var dataJson = JsonConvert.SerializeObject(new[]
                    {
                    new
                    {
                        botcid = data.id.ToString(),
                        ghichu = data.ghichu,
                        noidungbaocao = data.noidungbaocao,
                        ngaybaocao = data.ngaybaocao,
                        trangthaibaocao = data.trangthaibaocao,
                        filekiemchung = "keyupload",
                    }
                });
                    request.AddParameter("data", dataJson);

                    string test = HttpContext.Current.Server.MapPath("~/DesktopModules/FileUpload/BaoCao/" + data.filekiemchung);

                    request.AddFile("keyupload", test);
                } else
                {
                    request.AddHeader("Token", token);
                    request.AlwaysMultipartFormData = true;
                    request.AddParameter("serviceid", "ChDSB4MYYasSatFNFD4mCw==");
                    request.AddParameter("key", "[\"ghichu\", \"noidungbaocao\",\"ngaybaocao\",\"trangthaibaocao\"]");

                    var dataJson = JsonConvert.SerializeObject(new[]
                    {
                    new
                    {
                        botcid = data.id.ToString(),
                        ghichu = data.ghichu,
                        noidungbaocao = data.noidungbaocao,
                        ngaybaocao = data.ngaybaocao,
                        trangthaibaocao = data.trangthaibaocao,
                    }
                });
                    request.AddParameter("data", dataJson);
                }

                var response = await client.ExecuteTaskAsync(request);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var jsonResponse = response.Content;
                    return Request.CreateResponse(HttpStatusCode.OK, jsonResponse, "application/json");
                }
                else
                {
                    var errorResponse = response.Content;
                    return Request.CreateResponse(HttpStatusCode.BadRequest, errorResponse, "application/json");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Lỗi: " + ex.Message, "application/json");
            }
        }

        [HttpGet]
        public async Task<HttpResponseMessage> DanhSachCapQuocGia()
        {
            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                       | SecurityProtocolType.Tls11
                       | SecurityProtocolType.Tls12
                       | SecurityProtocolType.Ssl3;
                var client = new RestClient("https://sohoa.thuathienhue.gov.vn/data/danhsach");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("token", "5d26ef51-a0ac-4b47-8021-c88a621e5df6");
                request.AddHeader("Content-Type", "application/json");
                var body = @"{
                    " + "\n" +
                                    @"""serviceid"": ""ChDSB4MYYasSatFNFD4mCw=="",
                    " + "\n" +
                                    @"""thamso"": {
                    " + "\n" +
                                    @"	""tukhoa"": """",
                    " + "\n" +
                                    @"	""nambaocao"": ""0"",
                    " + "\n" +
                                    @"	""bochiso"": ""0"",
                    " + "\n" +
                                    @"	""nhomtieuchiid"": ""0"",
                    " + "\n" +
                                    @"	""tieuchiid"": ""0"",
                    " + "\n" +
                                    @"	""tieuchithanhphan"": ""0"",
                    " + "\n" +
                                    @"	""donvithuchien"": ""0"",
                    " + "\n" +
                                    @"	""donviduyet"": ""0"",
                    " + "\n" +
                                    @"	""hinhthuc"": ""0"",
                    " + "\n" +
                                    @"	""doituong"": ""0"",
                    " + "\n" +
                                    @"	""capapdung"": ""1"",
                    " + "\n" +
                                    @"	""trangthaibaocao"": ""0"",
                    " + "\n" +
                                    @"	""trangthaicongviec"": ""0"",
                    " + "\n" +
                                    @"	""pheduyet"": ""0""
                    " + "\n" +
                                    @"	},
                    " + "\n" +
                                    @"""page"": ""1"",
                    " + "\n" +
                                    @"""perpage"": ""99999""
                    " + "\n" +
                                    @"}";
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                JObject jResponse = JObject.Parse(response.Content.ToString());
                return Request.CreateResponse(HttpStatusCode.OK, jResponse, "application/json");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Lỗi :" + ex.Message, "application/json");
            }
        }

        [HttpGet]
        public async Task<HttpResponseMessage> DanhSachCapSo(int year)
        {
            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                       | SecurityProtocolType.Tls11
                       | SecurityProtocolType.Tls12
                       | SecurityProtocolType.Ssl3;
                var client = new RestClient("https://sohoa.thuathienhue.gov.vn/data/danhsach");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("token", "5d26ef51-a0ac-4b47-8021-c88a621e5df6");
                request.AddHeader("Content-Type", "application/json");
                var body = @"{
                    " + "\n" +
                                    @"""serviceid"": ""ChDSB4MYYasSatFNFD4mCw=="",
                    " + "\n" +
                                    @"""thamso"": {
                    " + "\n" +
                                    @"	""tukhoa"": """",
                    " + "\n" +
                                    @"	""nambaocao"": """+ year + @""",
                    " + "\n" +
                                    @"	""bochiso"": ""0"",
                    " + "\n" +
                                    @"	""nhomtieuchiid"": ""0"",
                    " + "\n" +
                                    @"	""tieuchiid"": ""0"",
                    " + "\n" +
                                    @"	""tieuchithanhphan"": ""0"",
                    " + "\n" +
                                    @"	""donvithuchien"": ""0"",
                    " + "\n" +
                                    @"	""donviduyet"": ""0"",
                    " + "\n" +
                                    @"	""hinhthuc"": ""0"",
                    " + "\n" +
                                    @"	""doituong"": ""0"",
                    " + "\n" +
                                    @"	""capapdung"": ""2"",
                    " + "\n" +
                                    @"	""trangthaibaocao"": ""0"",
                    " + "\n" +
                                    @"	""trangthaicongviec"": ""0"",
                    " + "\n" +
                                    @"	""pheduyet"": ""0""
                    " + "\n" +
                                    @"	},
                    " + "\n" +
                                    @"""page"": ""1"",
                    " + "\n" +
                                    @"""perpage"": ""99999""
                    " + "\n" +
                                    @"}";
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                JObject jResponse = JObject.Parse(response.Content.ToString());
                return Request.CreateResponse(HttpStatusCode.OK, jResponse, "application/json");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Lỗi :" + ex.Message, "application/json");
            }
        }

        [HttpGet]
        public async Task<HttpResponseMessage> DanhSachCapHuyen(int year)
        {
            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                       | SecurityProtocolType.Tls11
                       | SecurityProtocolType.Tls12
                       | SecurityProtocolType.Ssl3;
                var client = new RestClient("https://sohoa.thuathienhue.gov.vn/data/danhsach");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("token", "5d26ef51-a0ac-4b47-8021-c88a621e5df6");
                request.AddHeader("Content-Type", "application/json");
                var body = @"{
                    " + "\n" +
                                    @"""serviceid"": ""ChDSB4MYYasSatFNFD4mCw=="",
                    " + "\n" +
                                    @"""thamso"": {
                    " + "\n" +
                                    @"	""tukhoa"": """",
                    " + "\n" +
                                    @"	""nambaocao"": """+ year + @""",
                    " + "\n" +
                                    @"	""bochiso"": ""0"",
                    " + "\n" +
                                    @"	""nhomtieuchiid"": ""0"",
                    " + "\n" +
                                    @"	""tieuchiid"": ""0"",
                    " + "\n" +
                                    @"	""tieuchithanhphan"": ""0"",
                    " + "\n" +
                                    @"	""donvithuchien"": ""0"",
                    " + "\n" +
                                    @"	""donviduyet"": ""0"",
                    " + "\n" +
                                    @"	""hinhthuc"": ""0"",
                    " + "\n" +
                                    @"	""doituong"": ""0"",
                    " + "\n" +
                                    @"	""capapdung"": ""3"",
                    " + "\n" +
                                    @"	""trangthaibaocao"": ""0"",
                    " + "\n" +
                                    @"	""trangthaicongviec"": ""0"",
                    " + "\n" +
                                    @"	""pheduyet"": ""0""
                    " + "\n" +
                                    @"	},
                    " + "\n" +
                                    @"""page"": ""1"",
                    " + "\n" +
                                    @"""perpage"": ""99999""
                    " + "\n" +
                                    @"}";
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                JObject jResponse = JObject.Parse(response.Content.ToString());
                return Request.CreateResponse(HttpStatusCode.OK, jResponse, "application/json");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Lỗi :" + ex.Message, "application/json");
            }
        }

        [HttpGet]
        public async Task<HttpResponseMessage> DanhSachCapXa(int year)
        {
            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                       | SecurityProtocolType.Tls11
                       | SecurityProtocolType.Tls12
                       | SecurityProtocolType.Ssl3;
                var client = new RestClient("https://sohoa.thuathienhue.gov.vn/data/danhsach");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("token", "5d26ef51-a0ac-4b47-8021-c88a621e5df6");
                request.AddHeader("Content-Type", "application/json");
                var body = @"{
                    " + "\n" +
                                    @"""serviceid"": ""ChDSB4MYYasSatFNFD4mCw=="",
                    " + "\n" +
                                    @"""thamso"": {
                    " + "\n" +
                                    @"	""tukhoa"": """",
                    " + "\n" +
                                    @"	""nambaocao"": """+ year + @""",
                    " + "\n" +
                                    @"	""bochiso"": ""0"",
                    " + "\n" +
                                    @"	""nhomtieuchiid"": ""0"",
                    " + "\n" +
                                    @"	""tieuchiid"": ""0"",
                    " + "\n" +
                                    @"	""tieuchithanhphan"": ""0"",
                    " + "\n" +
                                    @"	""donvithuchien"": ""0"",
                    " + "\n" +
                                    @"	""donviduyet"": ""0"",
                    " + "\n" +
                                    @"	""hinhthuc"": ""0"",
                    " + "\n" +
                                    @"	""doituong"": ""0"",
                    " + "\n" +
                                    @"	""capapdung"": ""4"",
                    " + "\n" +
                                    @"	""trangthaibaocao"": ""0"",
                    " + "\n" +
                                    @"	""trangthaicongviec"": ""0"",
                    " + "\n" +
                                    @"	""pheduyet"": ""0""
                    " + "\n" +
                                    @"	},
                    " + "\n" +
                                    @"""page"": ""1"",
                    " + "\n" +
                                    @"""perpage"": ""99999""
                    " + "\n" +
                                    @"}";
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                JObject jResponse = JObject.Parse(response.Content.ToString());
                return Request.CreateResponse(HttpStatusCode.OK, jResponse, "application/json");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Lỗi :" + ex.Message, "application/json");
            }
        }

        [HttpGet]
        public async Task<HttpResponseMessage> DanhSachTieuChi()
        {
            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                       | SecurityProtocolType.Tls11
                       | SecurityProtocolType.Tls12
                       | SecurityProtocolType.Ssl3;
                var client = new RestClient("https://sohoa.thuathienhue.gov.vn/data/danhsach");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("token", "5d26ef51-a0ac-4b47-8021-c88a621e5df6");
                request.AddHeader("Content-Type", "application/json");
                var body = @"{
                    " + "\n" +
                                    @"""serviceid"": ""o7qr6ZfrbkERSilQ4xKK7g=="",
                    " + "\n" +
                                    @"""thamso"": {
                    " + "\n" +
                                    @"	""tukhoa"": """",
                    " + "\n" +
                                    @"	""bochisoid"": ""6"",                 
                    " + "\n" +
                                    @"	},
                    " + "\n" +
                                    @"""page"": ""1"",
                    " + "\n" +
                                    @"""perpage"": ""50""
                    " + "\n" +
                                    @"}";
                request.AddParameter("application/json", body, ParameterType.RequestBody);

                IRestResponse response = client.Execute(request);
                JObject jResponse = JObject.Parse(response.Content.ToString());

                //jResponse["data"] = new JArray(jResponse["data"].OrderByDescending(item => (string)item["tendonvithuchien"]));

                return Request.CreateResponse(HttpStatusCode.OK, jResponse, "application/json");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Lỗi :" + ex.Message, "application/json");
            }
        }

        [HttpGet]
        public async Task<HttpResponseMessage> DanhSachNamBaoCao()
        {
            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                       | SecurityProtocolType.Tls11
                       | SecurityProtocolType.Tls12
                       | SecurityProtocolType.Ssl3;
                var client = new RestClient("https://sohoa.thuathienhue.gov.vn/data/danhsach");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("token", "5d26ef51-a0ac-4b47-8021-c88a621e5df6");
                request.AddHeader("Content-Type", "application/json");
                var body = @"{
                    " + "\n" +
                                    @"""serviceid"": ""AUwayMD1hRAHt2aryqNfoQ=="",
                    " + "\n" +
                                    @"""thamso"": {
                    " + "\n" +
                                    @"	""tukhoa"": """",
                                    },
                    " + "\n" +
                                    @"""page"": ""1"",
                    " + "\n" +
                                    @"""perpage"": ""50""
                    " + "\n" +
                                    @"}";
                request.AddParameter("application/json", body, ParameterType.RequestBody);

                IRestResponse response = client.Execute(request);
                JObject jResponse = JObject.Parse(response.Content.ToString());

                //jResponse["data"] = new JArray(jResponse["data"].OrderByDescending(item => (string)item["tendonvithuchien"]));

                return Request.CreateResponse(HttpStatusCode.OK, jResponse, "application/json");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Lỗi :" + ex.Message, "application/json");
            }
        }

        //public string saveImagefromUrlDongBo(string url)
        //{
        //    string[] listSplit = url.Split('/');
        //    string nameImg = listSplit[listSplit.Length - 1];
        //    if (!System.IO.Directory.Exists(HttpContext.Current.Server.MapPath("~/AnhSanPham/AnhDongBo")))
        //    {
        //        System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/AnhSanPham/AnhDongBo"));
        //    }
        //    using (System.Net.WebClient client = new System.Net.WebClient())
        //    {
        //        client.DownloadFile(new Uri(url), HttpContext.Current.Server.MapPath("~/AnhSanPham/AnhDongBo/" + nameImg));
        //    }
        //    return "/AnhSanPham/AnhDongBo/" + nameImg;
        //}
    }
}