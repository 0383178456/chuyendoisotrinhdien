﻿using HueCIT.Modules.ChuyenDoiSoTrinhDien.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.IO;

namespace HueCIT.Modules.ChuyenDoiSoTrinhDien.Services
{
    [AllowAnonymous]
    public class UploadFileApiController : ApiBaseController
    {
        [HttpPost]
        public ApiResult<List<FileUploadModel>> UploadFiles(string pathFolder, string pathThuMuc)
        {
            HttpFileCollection files = HttpContext.Current.Request.Files;
            List<FileUploadModel> filesUrls = new List<FileUploadModel>();

            try
            {
                for (var i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    // files folder
                    var savePath = HttpContext.Current.Server.MapPath(pathFolder);
                    if (!Directory.Exists(savePath))
                        Directory.CreateDirectory(savePath);

                    string originname = Path.GetFileName(file.FileName);
                    string _path = Path.Combine(savePath, originname);
                    file.SaveAs(_path);

                    Uri baseuri = new Uri(Request.RequestUri.AbsoluteUri.Replace(Request.RequestUri.PathAndQuery, string.Empty));
                    var thuMuc = pathThuMuc;
                    var fileBase = $"{thuMuc}/{originname}";

                    FileUploadModel item = new FileUploadModel
                    {
                        ViTri = fileBase,
                        Ten = originname,
                        KichThuoc = file.ContentLength,
                        KieuMoRong = file.ContentType,
                    };

                    filesUrls.Add(item);
                }

                return new ApiSuccessResult<List<FileUploadModel>>(filesUrls, "Upload file thành công!");
            }
            catch (Exception)
            {
                return new ApiErrorResult<List<FileUploadModel>>("Upload file thất bại!");
            }
        }
    }
}