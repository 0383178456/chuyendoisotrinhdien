﻿using HueCIT.Modules.ChuyenDoiSoTrinhDien.Interface.DinhKemFiles;
using HueCIT.Modules.ChuyenDoiSoTrinhDien.Models;
using HueCIT.Modules.ChuyenDoiSoTrinhDien.Repository.DinhKemFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace HueCIT.Modules.ChuyenDoiSoTrinhDien.Services
{
    [AllowAnonymous]
    public class DinhKemApiController : ApiBaseController
    {
        private readonly IDinhKemRepository _repository;
        public DinhKemApiController()
        {
            _repository = new DinhKemRepository();
        }

        [HttpPost]
        public async Task<HttpResponseMessage> AddDinhKem(DinhKem data)
        {
            try
            {
                ApiResult<DinhKem> lst = new ApiResult<DinhKem>();
                lst = await _repository.AddDinhKem(data);
                return Request.CreateResponse(HttpStatusCode.OK, lst, "application/json");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Lỗi :" + ex.Message, "application/json");
            }
        }

        [HttpPost]
        public async Task<HttpResponseMessage> GetDinhkemGiaiPhap(Guid id)
        {
            try
            {
                ApiResult<List<DinhKem>> lst = new ApiResult<List<DinhKem>>();
                lst = await _repository.GetDinhkemGiaiPhap(id);
                return Request.CreateResponse(HttpStatusCode.OK, lst, "application/json");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Lỗi :" + ex.Message, "application/json");
            }
        }
    }
}