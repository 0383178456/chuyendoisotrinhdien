﻿$(document).ready(function () {
    $(".BaoCaoDTI .danhsachcoso .coso-details").each(function () {
        $(this).on("click", function () {
            $(this).siblings('.mini-list').slideToggle(500);
            if ($(this).hasClass('collapsed')) {
                $(this).removeClass('collapsed');
            } else {
                $(this).addClass('collapsed');
            }
        });
    });

    ChangeDisplay();

    $(".tab-filter > a").each(function () {
        $(this).click(function () {
            if ($(this).hasClass('active')) {
                $(".tab-filter a").removeClass("active");
                DisplayAll();
            } else {
                $(".tab-filter a").removeClass("active");
                $(this).addClass("active");
                ChangeDisplay();
            }
        })
    });

    function ChangeDisplay() {
        if ($('#CapSo').hasClass("active")) {
            $('.CapHuyen').css("display", "none");
            $('.CapXa').css("display", "none");
            $('.CapSo').css("display", "block");
        } if ($('#CapHuyen').hasClass("active")) {
            $('.CapSo').css("display", "none");
            $('.CapXa').css("display", "none");
            $('.CapHuyen').css("display", "block");
        } if ($('#CapXa').hasClass("active")) {
            $('.CapSo').css("display", "none");
            $('.CapHuyen').css("display", "none");
            $('.CapXa').css("display", "block");
        }
    }

    function DisplayAll() {
        $('.CapSo').css("display", "block");
        $('.CapHuyen').css("display", "block");
        $('.CapXa').css("display", "block");
    }
})