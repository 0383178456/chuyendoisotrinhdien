﻿using DotNetNuke.Web.Mvc.Framework.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HueCIT.Modules.ChuyenDoiSoTrinhDien.Controllers
{
    public class DangKyGiaiPhapController : DnnController
    {
        // GET: BaoCaoChiSo
        public ActionResult DangKyGiaiPhap(string id = "")
        {
            // VIEW CHI TIẾT SẢN PHẨM GIẢI PHÁP
            if (id != null && id != "")
            {
                ViewBag.IdGP = id;

                return View($"~/DesktopModules/MVC/ChuyenDoiSoTrinhDien/Views/DangKyGiaiPhap/DangKyGiaiPhapChiTiet.cshtml");
            }
            return View();
        }

        public ActionResult DangKyGiaiPhapChiTiet(/*string id = ""*/)
        {
            //// VIEW CHI TIẾT SẢN PHẨM GIẢI PHÁP
            //if (id != null && id != "")
            //{
            //    ViewBag.IdGP = id;

            //    return View($"~/DesktopModules/MVC/ChuyenDoiSoTrinhDien/Views/DangKyGiaiPhap/DangKyGiaiPhapChiTiet.cshtml");
            //}

            return View();
        }
    }
}