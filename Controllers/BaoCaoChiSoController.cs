﻿using DotNetNuke.Web.Mvc.Framework.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HueCIT.Modules.ChuyenDoiSoTrinhDien.Controllers
{
    public class BaoCaoChiSoController : DnnController
    {
        // GET: BaoCaoChiSo
        public ActionResult BaoCaoChiSo(string id = "", int capapdung = 0, int nam = 0)
        {
            if(id != "")
            {
                ViewBag.ItemMaDoVi = id;
                ViewBag.ItemCapApDung = capapdung;
                ViewBag.ItemNamBaoCao = nam;
                return View($"~/DesktopModules/MVC/ChuyenDoiSoTrinhDien/Views/BaoCaoChiSo/BaoCaoChiSoChiTiet.cshtml");
            }
            return View();
        }

        // GET: BaoCaoChiSo
        public ActionResult BaoCaoChiSoChiTiet(/*string url, string id = ""*/)
        {
            //if (id != "" && id != null)
            //{
            //    ViewBag.ItemMaDoVi = id;
            //}
            //return View($"~/DesktopModules/MVC/ChuyenDoiSoTrinhDien/Views/BaoCaoChiSo/BaoCaoChiSoChiTiet.cshtml");
            return View();
        }

        // GET: BaoCaoChiSo
        public ActionResult BaoCaoChiSoBaoCao(string url, string id = "")
        {
            if (id != "" && id != null)
            {
                ViewBag.ItemId = id;
            }

            string test = Server.MapPath("~/DesktopModules/MVC/ChuyenDoiSoTrinhDien/FileUpload/BaoCao/Test.docx"); ;

            return View($"~/DesktopModules/MVC/ChuyenDoiSoTrinhDien/Views/BaoCaoChiSo/{url}");
        }
    }
}