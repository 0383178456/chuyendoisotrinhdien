﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HueCIT.Modules.ChuyenDoiSoTrinhDien.Models
{
    public class DinhKem
    {
        public int IDTep { get; set; }
        public string TenTep { get; set; }
        public Guid IDChuThe { get; set; }
        public string MoTa { get; set; }
        public string LienKet { get; set; }
        public int Kieu { get; set; }
    }
}