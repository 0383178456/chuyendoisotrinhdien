﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HueCIT.Modules.ChuyenDoiSoTrinhDien.Models
{
    public class LinhVuc
    {
        public int IDLinhVuc { get; set; }
        public string TenLinhVuc { get; set; }
        public int SuDung { get; set; }
        public int CountIdLinhVuc { get; set; }
    }

    public class SearchLinhVuc
    {
        public string TuKhoa { get; set; }
    }
}