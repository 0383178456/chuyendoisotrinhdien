﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace HueCIT.Modules.ChuyenDoiSoTrinhDien.Models
{
    public class BaoCaoChiSoThuocTinh
    {
        public int sttbanghi { get; set; }
        public int id { get; set; }
        public int nambaocao { get; set; }
        public string tennambaocao { get; set; }
        public int bochiso { get; set; }
        public string tenbochiso { get; set; }
        public int nhomtieuchiid { get; set; }
        public string tennhomtieuchiid { get;set; }
        public int tieuchiid { get;set; }
        public string tentieuchiid { get;set; }
        public int tieuchithanhphan { get;set; }
        public string tentieuchithanhphan { get;set; }
        public string congvieccuthe { get;set; }
        public string huongdanthuchien { get;set; }
        public string cancu { get;set; }
        public string donvithuchien { get;set; }
        public string tendonvithuchien { get;set; }
        public string tendonviduyet { get;set; }
        public string donviduyet { get;set; }
        public DateTime thoigianhoanthanh { get;set; }
        public int hinhthuc { get;set; }
        public string tenhinhthuc { get;set; }
        public int doituong { get;set; }
        public string tendoituong { get;set; }
        public int kinhphidautucong { get;set; }
        public int kinhphisunghiep { get;set; }
        public int diemtoida { get;set; }
        public int diempheduyet { get;set; }
        public int capapdung { get;set; }
        public string tencapapdung { get;set; }
        public string dulieusosanh { get;set; }
        public string noidungbaocao { get;set; }
        public string filekiemchung { get;set; }
        public string ghichu { get;set; }
        public int trangthaibaocao { get;set; }
        public string tentrangthaibaocao { get;set; }
        public string filehuongdan { get;set; }
        public int trangthaicongviec { get;set; }
        public string tentrangthaicongviec { get;set; }
        public string noidungpheduyet { get;set; }
        public int pheduyet { get;set; }
        public string tenpheduyet { get;set; }
        public DateTime ngaycapnhat { get;set; }
        public DateTime ngaybaocao { get;set; }
    }

    public class BaoCaoChiSoThuocTinhParam
    {
        public int sttbanghi { get; set; }
        public int id { get; set; }
        public int nambaocao { get; set; }
        public string tennambaocao { get; set; }
        public int bochiso { get; set; }
        public string tenbochiso { get; set; }
        public int nhomtieuchiid { get; set; }
        public string tennhomtieuchiid { get; set; }
        public int tieuchiid { get; set; }
        public string tentieuchiid { get; set; }
        public int tieuchithanhphan { get; set; }
        public string tentieuchithanhphan { get; set; }
        public string congvieccuthe { get; set; }
        public string huongdanthuchien { get; set; }
        public string cancu { get; set; }
        public string donvithuchien { get; set; }
        public string tendonvithuchien { get; set; }
        public string tendonviduyet { get; set; }
        public string donviduyet { get; set; }
        public DateTime thoigianhoanthanh { get; set; }
        public int hinhthuc { get; set; }
        public string tenhinhthuc { get; set; }
        public int doituong { get; set; }
        public string tendoituong { get; set; }
        public int kinhphidautucong { get; set; }
        public int kinhphisunghiep { get; set; }
        public int diemtoida { get; set; }
        public int diempheduyet { get; set; }
        public int capapdung { get; set; }
        public string tencapapdung { get; set; }
        public string dulieusosanh { get; set; }
        public string noidungbaocao { get; set; }
        public string filekiemchung { get; set; }
        public string ghichu { get; set; }
        public int trangthaibaocao { get; set; }
        public string tentrangthaibaocao { get; set; }
        public string filehuongdan { get; set; }
        public int trangthaicongviec { get; set; }
        public string tentrangthaicongviec { get; set; }
        public string noidungpheduyet { get; set; }
        public int pheduyet { get; set; }
        public string tenpheduyet { get; set; }
        public string ngaycapnhat { get; set; }
        public string ngaybaocao { get; set; }
    }
}