﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HueCIT.Modules.ChuyenDoiSoTrinhDien.Models
{
    
    //Name: DongLHP;
    //Date: 10-07-2023;
    //Function: Model đăng ký giải pháp;

    public class DangKyGiaiPhapProperties
    {
        public Guid ID { get; set; }
        public string TenUngDung { get; set; }
        public string AnhDaiDien { get; set; }
        public int IDLinhVuc { get; set; }
        public string GioiThieu { get; set; }
        public string TinhNangNoiBat { get; set; }
        public string AnhSanPham { get; set; }
        public string DiaChi { get; set; }
        public string DienThoai { get; set; }
        public string HopThu { get; set; }
        public string LogoDonVi { get; set; }
        public string TenDonVi { get; set; }
        public string LinkGooglePlay { get; set; }
        public string LinkAppStore { get; set; }
        public string LinkDonVi { get; set; }
        public string LinkUngDung { get; set; }
    }

    public class DangKyFilterLinhVuc
    {
        public int IdLinhVuc { get; set; }
    }

    public class DangKyGiaiPhapGet
    {
        public Guid ID { get; set; }
        public string TenUngDung { get; set; }
        public string AnhDaiDien { get; set; }
        public int IDLinhVuc { get; set; }
        public string GioiThieu { get; set; }
        public string TinhNangNoiBat { get; set; }
        public string AnhSanPham { get; set; }
        public string DiaChi { get; set; }
        public string DienThoai { get; set; }
        public string HopThu { get; set; }
        public int TrangThai { get; set; }
        public bool TieuDiem { get; set; }
        public DateTime ThoiGianDuyet { get; set; }
        public string LogoDonVi { get; set; }
        public string TenDonVi { get; set; }
        public string LinkGooglePlay { get; set; }
        public string LinkAppStore { get; set; }
        public string LinkDonVi { get; set; }
        public string LinkUngDung { get; set; }

        public string TenLinhVuc { get; set; }
    }

    public class ModelSSO
    {
        public string MaDonViSSO { get; set; }
        public string Username { get; set; }
        public List<string> RoleName { get; set; } = new List<string>();
        public List<int> RoleId { get; set; } = new List<int>();

    }
}