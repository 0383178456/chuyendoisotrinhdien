﻿using HueCIT.Modules.ChuyenDoiSoTrinhDien.Interface.DangKyGiaiPhap;
using HueCIT.Modules.ChuyenDoiSoTrinhDien.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace HueCIT.Modules.ChuyenDoiSoTrinhDien.Repository.DangKyGiaiPhap
{
    public class DangKyGiaiPhapRepository : ConnectDatabase, IDangKyGiaiPhapRepository
    {
        public async Task<ApiResult<DangKyGiaiPhapProperties>> AddDangKyGiaiPhap(DangKyGiaiPhapProperties data)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@TenUngDung", data.TenUngDung);
                    parameters.Add("@AnhDaiDien", data.AnhDaiDien);
                    parameters.Add("@IDLinhVuc", data.IDLinhVuc);
                    parameters.Add("@GioiThieu", data.GioiThieu);
                    parameters.Add("@TinhNangNoiBat", data.TinhNangNoiBat);
                    parameters.Add("@DiaChi", data.DiaChi);
                    parameters.Add("@DienThoai", data.DienThoai);
                    parameters.Add("@HopThu", data.HopThu);
                    parameters.Add("@LogoDonVi", data.LogoDonVi);
                    parameters.Add("@TenDonVi", data.TenDonVi);
                    parameters.Add("@LinkGooglePlay", data.LinkGooglePlay);
                    parameters.Add("@LinkAppStore", data.LinkAppStore);
                    parameters.Add("@LinkDonVi", data.LinkDonVi);
                    parameters.Add("@LinkUngDung", data.LinkUngDung);

                    DangKyGiaiPhapProperties list = conn.QueryFirstOrDefault<DangKyGiaiPhapProperties>("SP_CDS_DangKyGiaiPhap_Add", parameters, commandType: CommandType.StoredProcedure);

                    return new ApiSuccessResult<DangKyGiaiPhapProperties>
                    {
                        IsSuccessed = true,
                        Message = "Đăng ký giải pháp thành công",
                        ResultObj = list
                    };
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<ApiResult<List<DangKyGiaiPhapGet>>> GetsDangKyGiaiPhap(DangKyFilterLinhVuc data)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();

                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@IdLinhVuc", data.IdLinhVuc);

                    List<DangKyGiaiPhapGet> list = conn.Query<DangKyGiaiPhapGet>("SP_CDS_DangKyGiaiPhap_Gets_TrangThai", parameters, commandType: CommandType.StoredProcedure).ToList();

                    return new ApiSuccessResult<List<DangKyGiaiPhapGet>>
                    {
                        IsSuccessed = true,
                        Message = "Lấy thông đăng ký thành công!",
                        ResultObj = list,
                    };
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<List<DangKyGiaiPhapGet>>(ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<ApiResult<List<LinhVuc>>> GetsLinhVuc(SearchLinhVuc data)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();

                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@TuKhoa", data.TuKhoa);

                    List<LinhVuc> list = conn.Query<LinhVuc>("SP_LinhVucGiaiPhap_Gets", parameters, commandType: CommandType.StoredProcedure).ToList();

                    return new ApiSuccessResult<List<LinhVuc>>
                    {
                        IsSuccessed = true,
                        Message = "Lấy thông tin lĩnh vực thành công!",
                        ResultObj = list,
                    };
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<List<LinhVuc>>(ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<ApiResult<List<LinhVuc>>> GetsLinhVuc_GiaiPhap()
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();

                    List<LinhVuc> list = conn.Query<LinhVuc>("SP_CDS_LinhVuc_Get_GiaiPhap", commandType: CommandType.StoredProcedure).ToList();

                    return new ApiSuccessResult<List<LinhVuc>>
                    {
                        IsSuccessed = true,
                        Message = "Lấy thông tin lĩnh vực thành công!",
                        ResultObj = list,
                    };
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<List<LinhVuc>>(ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<ApiResult<DangKyGiaiPhapGet>> GetGiaiPhapNew()
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();

                    DangKyGiaiPhapGet list = conn.QueryFirstOrDefault<DangKyGiaiPhapGet>("SP_CDS_GiaiPhapDoanhNghiep_GetNew", commandType: CommandType.StoredProcedure);

                    return new ApiSuccessResult<DangKyGiaiPhapGet>
                    {
                        IsSuccessed = true,
                        Message = "Lấy thông tin giải pháp mới nhất thành công!",
                        ResultObj = list,
                    };
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<DangKyGiaiPhapGet>(ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<ApiResult<List<DangKyGiaiPhapGet>>> GetsGiaiPhapTieuDiem()
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();

                    List<DangKyGiaiPhapGet> list = conn.Query<DangKyGiaiPhapGet>("SP_CDS_GiaiPhapDoanhNghiep_Get_TieuDiem", commandType: CommandType.StoredProcedure).ToList();

                    return new ApiSuccessResult<List<DangKyGiaiPhapGet>>
                    {
                        IsSuccessed = true,
                        Message = "Lấy thông tin giải pháp nổi bật thành công!",
                        ResultObj = list,
                    };
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<List<DangKyGiaiPhapGet>>(ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<ApiResult<DangKyGiaiPhapGet>> GetSanPhamGiaiPhap(Guid id)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@ID", id);

                    DangKyGiaiPhapGet list = conn.QueryFirstOrDefault<DangKyGiaiPhapGet>("SP_CDS_GiaiPhapSanPham_Get",parameters,commandType: CommandType.StoredProcedure);

                    return new ApiSuccessResult<DangKyGiaiPhapGet>
                    {
                        IsSuccessed = true,
                        Message = "Lấy thông tin giải pháp thành công!",
                        ResultObj = list,
                    };
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<DangKyGiaiPhapGet>(ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
    }
}