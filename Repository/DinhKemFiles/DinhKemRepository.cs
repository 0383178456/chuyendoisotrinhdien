﻿using Dapper;
using HueCIT.Modules.ChuyenDoiSoTrinhDien.Interface.DinhKemFiles;
using HueCIT.Modules.ChuyenDoiSoTrinhDien.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HueCIT.Modules.ChuyenDoiSoTrinhDien.Repository.DinhKemFiles
{
    public class DinhKemRepository : ConnectDatabase, IDinhKemRepository
    {
        public async Task<ApiResult<DinhKem>> AddDinhKem(DinhKem data)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@TenTep", data.TenTep);
                    parameters.Add("@IDChuThe", data.IDChuThe);
                    parameters.Add("@MoTa", data.MoTa);
                    parameters.Add("@LienKet", data.LienKet);
                    parameters.Add("@Kieu", data.Kieu);

                    DinhKem list = conn.QueryFirstOrDefault<DinhKem>("SP_CDS_DinhKemFiles_Add", parameters, commandType: CommandType.StoredProcedure);

                    return new ApiSuccessResult<DinhKem>
                    {
                        IsSuccessed = true,
                        Message = "Tải tệp thành công",
                        ResultObj = list
                    };
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<ApiResult<List<DinhKem>>> GetDinhkemGiaiPhap(Guid id)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@IdChuThe", id);

                    List<DinhKem> list = conn.Query<DinhKem>("SP_CDS_TepDinhKem_Get_Giaiphap", parameters, commandType: CommandType.StoredProcedure).ToList();

                    return new ApiSuccessResult<List<DinhKem>>
                    {
                        IsSuccessed = true,
                        Message = "Lấy đính kèm giải pháp thành công!",
                        ResultObj = list,
                    };
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<List<DinhKem>>(ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
    }
}