﻿using HueCIT.Modules.ChuyenDoiSoTrinhDien.Components;
using HueCIT.Modules.ChuyenDoiSoTrinhDien.Interface;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HueCIT.Modules.ChuyenDoiSoTrinhDien.Repository
{
    public class ConnectDatabase : IConnectDatabase
    {
        public SqlConnection IConnectData()
        {
            try
            {
                var conn = new SqlConnection
                {
                    //ConnectionString = @"Data Source=14.225.23.91;Initial Catalog=qlkhcn;User ID=qlkhcn;Password=B1nhB0t#"
                    ConnectionString = CauHinhHeThong.Connection
                };

                return conn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}