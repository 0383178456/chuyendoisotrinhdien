﻿using System.Collections.Generic;
//using System.Xml;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Search;
using DotNetNuke.Web.Api;

namespace HueCIT.Modules.ChuyenDoiSoTrinhDien.Components
{
    public class RouterMapper : IServiceRouteMapper
    {
        public void RegisterRoutes(IMapRoute mapRouteManager)
        {
            mapRouteManager.MapHttpRoute(
            "ChuyenDoiSoTrinhDien",
            "default",
            "{controller}/{action}",
            new[] { "HueCIT.Modules.ChuyenDoiSoTrinhDien.Services" });
        }
    }
}
