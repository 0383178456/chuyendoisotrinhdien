﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using HueCIT.Modules.ChuyenDoiSoTrinhDien.Models;

namespace HueCIT.Modules.ChuyenDoiSoTrinhDien.Interface.DinhKemFiles
{
    internal interface IDinhKemRepository
    {
        Task<ApiResult<DinhKem>> AddDinhKem(DinhKem data);
        Task<ApiResult<List<DinhKem>>> GetDinhkemGiaiPhap(Guid id);
    }
}