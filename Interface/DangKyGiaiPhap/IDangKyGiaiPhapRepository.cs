﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using HueCIT.Modules.ChuyenDoiSoTrinhDien.Models;

namespace HueCIT.Modules.ChuyenDoiSoTrinhDien.Interface.DangKyGiaiPhap
{
    internal interface IDangKyGiaiPhapRepository
    {
        //GETS GIẢI PHÁP ĐÃ ĐĂNG KÝ VÀ ĐƯỢC QUẢN TRỊ DUYỆT
        Task<ApiResult<List<DangKyGiaiPhapGet>>> GetsDangKyGiaiPhap(DangKyFilterLinhVuc data);
        //ADD GIẢI PHÁP
        Task<ApiResult<DangKyGiaiPhapProperties>> AddDangKyGiaiPhap(DangKyGiaiPhapProperties data);
        //GETS LĨNH VỰC
        Task<ApiResult<List<LinhVuc>>> GetsLinhVuc(SearchLinhVuc data);
        //GETS LĨNH VỰC ĐÃ ĐĂNG KÝ GIẢI PHÁP
        Task<ApiResult<List<LinhVuc>>> GetsLinhVuc_GiaiPhap();
        //GET GIẢI PHÁP NỔI BẬT
        Task<ApiResult<List<DangKyGiaiPhapGet>>> GetsGiaiPhapTieuDiem();
        //GET GIẢI PHÁP DUYỆT MỚI NHẤT
        Task<ApiResult<DangKyGiaiPhapGet>> GetGiaiPhapNew();
        //GET GIẢI PHÁP THEO ID
        Task<ApiResult<DangKyGiaiPhapGet>> GetSanPhamGiaiPhap(Guid id);
    }
}