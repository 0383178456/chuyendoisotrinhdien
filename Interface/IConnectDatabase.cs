﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace HueCIT.Modules.ChuyenDoiSoTrinhDien.Interface
{
    internal interface IConnectDatabase
    {
        SqlConnection IConnectData();
    }
}